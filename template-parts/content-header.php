<?php

/**
 * @author  wpWax
 * @author   1.0
 * @version 1.0
 */

use wpWax\dLawyers\Directorist_Support;
use wpWax\dLawyers\Helper;
use wpWax\dLawyers\Theme;

$nav_menu_args      = Helper::get_nav_menu_args('button');
$logo              = Helper::get_logo_src();
$menu_align       = ((Theme::$options['add_listing_button'] || Theme::$options['header_search'] || Theme::$options['header_search']) && class_exists('Directorist_Base')) ? '' : ' menu-right';
$container_type    = (Theme::$options['container_type']) ? Theme::$options['container_type'] : 'container';
?>
<div class="theme-header-area">

  <div class="<?php echo esc_attr(apply_filters('dlawyers_header_container_class', $container_type)); ?>">

    <div class="theme-header-inner flex items-center">

      <div class="theme-header-logo">

        <div class="theme-header-logo__brand site-branding">

          <a class="theme-header-logo__img navbar-brand order-sm-1 order-1" href="<?php echo esc_url(home_url('/')); ?>">

            <img src="<?php echo esc_url($logo); ?>" alt="<?php esc_attr(bloginfo('name')); ?>">

          </a>

        </div>

      </div>

      <div class="theme-header-menu">

        <div class="d_menu main-navigation<?php echo esc_attr($menu_align); ?>">

          <?php wp_nav_menu($nav_menu_args); ?>

        </div>

      </div>

      <?php if (class_exists('Directorist_Base') && (Theme::$options['header_search'] || Theme::$options['header_account'] || Theme::$options['add_listing_button'])) : ?>

        <div class="theme-header-action">

          <ul class="theme-header-action__author d-flex list-unstyled align-items-center">

            <?php if (Theme::$options['header_search']) : ?>

              <li class="theme-header-action__search">

                <div class="theme-header-action__search--trigger">

                  <span id="search-icon" class="d-flex search-icon-wrapper"><i class="search-icon themeicon themeicon-search-solid fw-400"></i></span>

                </div>

              </li>

            <?php endif; ?>

            <?php
            if (is_page(get_directorist_option('user_dashboard'))) {
              Directorist_Support::get_header_listing_button_html();
            }
            ?>

            <?php
            if (Theme::$options['header_account']) {
              Helper::get_template_part('directorist/custom/header-account');
            }
            ?>

            <?php
            if (!is_page(get_directorist_option('user_dashboard'))) {
              Directorist_Support::get_header_listing_button_html();
            }
            ?>

          </ul>

        </div>

      <?php endif; ?>

    </div>

  </div>

</div>

<?php Helper::get_template_part('directorist/custom/content-header-login-modal'); ?>