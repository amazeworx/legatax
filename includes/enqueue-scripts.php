<?php

/**
 * Enqueue theme assets.
 */
function tailpress_enqueue_scripts()
{
  $theme = wp_get_theme();

  wp_enqueue_style('dlawyers-child', get_stylesheet_uri());
  wp_enqueue_style('unicons', 'https://unicons.iconscout.com/release/v4.0.0/css/line.css', array(), '4.0.0');
  wp_enqueue_style('swiper7', 'https://unpkg.com/swiper@7/swiper-bundle.min.css', array(), '7.4.1');

  wp_enqueue_style('legatax-app', tailpress_asset('css/app.css'), array(), $theme->get('Version'));
  wp_enqueue_script('swiper7', 'https://unpkg.com/swiper@7/swiper-bundle.min.js', array(), '7.4.1');
  //wp_enqueue_script('legatax-app', tailpress_asset('js/app.js'), array(), $theme->get('Version'));
}
add_action('wp_enqueue_scripts', 'tailpress_enqueue_scripts', 18);

/**
 * Get asset path.
 *
 * @param string  $path Path to asset.
 *
 * @return string
 */
function tailpress_asset($path)
{
  if (wp_get_environment_type() === 'production') {
    return get_stylesheet_directory_uri() . '/' . $path;
  }

  return add_query_arg('time', time(),  get_stylesheet_directory_uri() . '/' . $path);
}
