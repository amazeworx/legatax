<?php

function legatax_lawyer_carousel_shortcode()
{
  $output = '';
  $args = array(
    'post_type' => 'at_biz_dir',
    'posts_per_page' => 8
    // 'meta_query' => array(
    //   array(
    //     'key'     => '_featured',
    //     'value'   => '1',
    //     'compare' => '=',
    //   ),
    // ),
  );
  $the_query = new WP_Query($args);

  if ($the_query->have_posts()) {
    //$output .= '<div class="grid grid-cols-3 gap-8">';
    $output .= '<div id="lawyers-carousel" class="swiper">';
    $output .= '<div class="swiper-wrapper">';
    while ($the_query->have_posts()) {
      $the_query->the_post();
      $the_title = get_the_title();
      $the_permalink = get_the_permalink();
      $listing_prv_img = get_post_meta(get_the_ID(), '_listing_prv_img', true);
      $educations = get_post_meta(get_the_ID(), '_custom-bullet-list', true);
      $the_education = explode("\n", $educations);
      $the_education = $the_education['0'];

      $categories = get_the_terms(get_the_ID(), 'at_biz_dir-category');
      $the_categories = join(', ', wp_list_pluck($categories, 'name'));

      $location_list = get_the_terms(get_the_ID(), 'at_biz_dir-location');
      $the_location = $location_list[0]->name;

      $the_image = wp_get_attachment_image($listing_prv_img, 'thumbnail', '', array('class' => 'w-20 h-20 object-cover'));

      $output .= '<div class="swiper-slide">';
      $output .= '<a href="' . $the_permalink . '" class="flex flex-col shadow rounded p-6 transition duration-300 hover:shadow-lg">';
      $output .= '<div class="rounded-full mb-2 w-20 h-20 overflow-hidden">';
      $output .= $the_image;
      $output .= '</div>';
      $output .= '<div class="flex flex-col">';
      $output .= '<h4 class="font-bold text-xl text-gray-800 mb-2">' . $the_title . '</h4>';
      $output .= '<div class="text-gray-500 text-sm mb-3">';
      $output .= $the_categories;
      $output .= '</div>';
      $output .= '</div>';
      $output .= '<ul class="mt-auto text-gray-500 text-sm">';
      $output .= '<li>';
      $output .= '<div class="flex"><i class="uil uil-map-marker text-lg leading-none flex-none mr-1 text-indigo-700"></i><span class="block">' . $the_location . '</span></div>';
      $output .= '</li>';
      $output .= '</ul>';
      $output .= '</a>';
      $output .= '</div>';
    }
    $output .= '</div>';
    $output .= '<div class="swiper-pagination"></div>';
    $output .= '</div>';
    //$output .= '</div>';

    $output .= '<script>';
    $output .= 'const swiper = new Swiper("#lawyers-carousel", {
      slidesPerView: "auto",
      spaceBetween: 40,
      loop: true,
      autoplay: {
        delay: 3000,
        disableOnInteraction: false,
      },
      pagination: {
        el: ".swiper-pagination",
        clickable: true,
      },
    });';
    $output .= '</script>';
  }
  wp_reset_postdata();

  return $output;
}
add_shortcode('lawyers_carousel', 'legatax_lawyer_carousel_shortcode');
