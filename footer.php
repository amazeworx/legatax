<?php

/**
 * @author  wpWax
 * @since   1.0
 * @version 1.0
 */

use \wpWax\dLawyers\Theme;

$footer_columns = 0;

foreach (range(1, 4) as $i) {
  if (is_active_sidebar('footer-' . $i)) {
    $footer_columns++;
  }
}

switch ($footer_columns) {
  case '1':
    $footer_class = 'w-full ';
    break;
  case '2':
    $footer_class = 'w-1/2';
    break;
  case '3':
    $footer_class = 'w-full md:w-1/3 ';
    break;
  default:
    $footer_class = 'w-full md:w-1/2 lg:w-1/4';
    break;
}
?>
</div><!-- #content -->
<footer class="site-footer">

  <?php if (Theme::$options['footer_area'] && $footer_columns) : ?>

    <div class="footer-top-area">

      <div class="container">

        <div class="flex gap-4 lg:gap-8">

          <?php foreach (range(1, 4) as $i) :

            if (!is_active_sidebar('footer-' . $i)) {
              continue;
            }
          ?>

            <div class="<?php echo esc_attr($footer_class); ?>">

              <?php dynamic_sidebar('footer-' . $i); ?>

            </div>

          <?php endforeach; ?>

        </div>

      </div>

    </div>

  <?php endif; ?>

  <?php if (Theme::$options['copyright_area']) : ?>

    <div class="footer-bottom-area">

      <div class="container">

        <div class="row">

          <div class="col-md-12">

            <div class="copyright-text"><?php echo wp_kses_post(Theme::$options['copyright_text']); ?></div>

          </div>

        </div>

      </div>

    </div>

  <?php endif; ?>

</footer>

</div>

<?php wp_footer(); ?>

</body>

</html>