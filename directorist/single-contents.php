<?php

/**
 * @author  wpWax
 * @since   6.7
 * @version 7.0.5
 */

use \Directorist\Directorist_Single_Listing;
use \Directorist\Helper;

if (!defined('ABSPATH')) exit;

$listing = Directorist_Single_Listing::instance();

// if (rcp_user_can_access(get_current_user_id(), get_the_ID())) {
//   // user can access
//   echo get_current_user_id();
// } else {
//   // user cannot access
//   echo get_current_user_id();
// }


// if (rcp_user_has_access()) {
//   echo get_current_user_id();
//   echo 'User can access';
// } else {
//   echo 'User cant access';
// }

?>

<div class="directorist-single-contents-area directorist-w-100">
  <div class="container">
    <div class="mt-3">
      <?php $listing->notice_template(); ?>
    </div>

    <div class="<?php Helper::directorist_row(); ?>">

      <div class="col-md-12">

        <?php if ($listing->current_user_is_author() || $listing->submit_link() || $listing->display_back_link()) : ?>

          <?php Helper::get_template('single/top-actions'); ?>

        <?php endif; ?>

        <?php $listing->header_template(); ?>
      </div>

      <div class="<?php Helper::directorist_single_column(); ?>">

        <div class="directorist-single-wrapper">

          <?php
          foreach ($listing->content_data as $section) {
            $listing->section_template($section);
          }
          ?>

        </div>

      </div>

      <?php Helper::get_template('single-sidebar'); ?>

    </div>
  </div>
</div>