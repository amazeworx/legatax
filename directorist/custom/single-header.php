<?php
/**
 * @author  wpWax
 * @since   1.0
 * @version 1.0
 */

use Directorist\Directorist_Single_Listing;
use Directorist\Helper;
use wpWax\dLawyers\Directorist_Support;
use wpWax\dLawyers\Helper as DHelper;

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

$listing                        = Directorist_Single_Listing::instance();
$dlawyers_display_gender        = ! empty( $listing->header_data['options']['content_settings']['listing_title']['enable_gender'] ) ? $listing->header_data['options']['content_settings']['listing_title']['enable_gender'] : '';
$dlawyers_display_age           = ! empty( $listing->header_data['options']['content_settings']['listing_title']['enable_age'] ) ? $listing->header_data['options']['content_settings']['listing_title']['enable_age'] : '';
$dlawyers_display_title         = ! empty( $listing->header_data['options']['content_settings']['listing_title']['enable_title'] ) ? $listing->header_data['options']['content_settings']['listing_title']['enable_title'] : '';
$dlawyers_display_tagline       = ! empty( $listing->header_data['options']['content_settings']['listing_title']['enable_tagline'] ) ? $listing->header_data['options']['content_settings']['listing_title']['enable_tagline'] : '';
$dlawyers_display_content       = ! empty( $listing->header_data['options']['content_settings']['listing_title']['enable_content'] ) ? $listing->header_data['options']['content_settings']['listing_title']['enable_content'] : '';
$dlawyers_display_claim_listing = ! empty( $listing->header_data['listings_header']['thumbnail'][0]['enable_claim_listing'] ) ? $listing->header_data['listings_header']['thumbnail'][0]['enable_claim_listing'] : '';

$dlawyers_display_reviews   = ! empty( $listing->header_data['options']['content_settings']['listing_title']['enable_reviews'] ) ? $listing->header_data['options']['content_settings']['listing_title']['enable_reviews'] : '';
$dlawyers_review_count_html = sprintf( _nx( '<span> %s </span> Review', '<span> %s </span> Reviews', $listing->get_review_count(), 'review count', 'dlawyers' ), $listing->get_review_count() );


$dlawyers_rating_count      = $listing->get_rating_count() ? ceil( $listing->get_rating_count() ) : 0;

$dlawyers_display_consultation      = ! empty( $listing->header_data['options']['content_settings']['listing_title']['enable_consultation'] ) ? $listing->header_data['options']['content_settings']['listing_title']['enable_consultation'] : '';
$dlawyers_display_consultation_text = ! empty( $listing->header_data['options']['content_settings']['listing_title']['enable_consultation_text'] ) ? $listing->header_data['options']['content_settings']['listing_title']['enable_consultation_text'] : '';
$dlawyers_display_consultation_text = $dlawyers_display_consultation_text ? $dlawyers_display_consultation_text : esc_html__( 'Free Consultation', 'dlawyers' );
$dlawyers_display_consultation_icon = 'uil uil-check';

$dlawyers_get_consultation = get_post_meta( $listing->id, '_dlawyers_free_consultation', true );
$dlawyers_get_gender       = get_post_meta( $listing->id, '_dlawyers_gender', true );
$dlawyers_get_age          = get_post_meta( $listing->id, '_dlawyers_age', true );
?>

<div class="theme-listing-details-card__body">

	<div class="theme-listing-details-card__module">

		<div class="theme-listing-details-card__left">

			<div class="theme-listing-about-details">

				<div class="theme-listing-about-details__img">

					<?php
					if( $listing->slider_template() ){
						$listing->slider_template();
					}
					?>

					<?php if ( class_exists( 'DCL_Base' ) && $dlawyers_display_claim_listing ): ?>

						<?php DHelper::get_template_part( 'directorist/custom/claim-listing-template' );?>

					<?php endif;?>

				</div>
				
				<div class="theme-listing-about-details__title">

					<?php if ( $dlawyers_display_title ): ?>

						<div class="theme-listing-about-details__title--title">

							<h2><?php echo esc_html( $listing->get_title() ); ?></h2>

							<?php do_action( 'directorist_single_listing_after_title', $listing->id );?>

						</div>

					<?php endif;?>

					<div class="theme-listing-about-details__separator">
					
						<?php if ( $dlawyers_display_tagline && $listing->get_tagline() ): ?>

							<span class="directorist-listing-details-tagline">

								<?php echo esc_html( $listing->get_tagline() ); ?>
				
							</span>
							
						<?php endif;?>
						
						<?php if ( $dlawyers_display_gender && $dlawyers_get_gender ): ?>

							<span class="directorist-listing-details-gender">

								<?php echo esc_html( $dlawyers_get_gender ? $dlawyers_get_gender : '' ); ?>
							
							</span>

						<?php endif;?>

						<?php if ( $dlawyers_display_age && $dlawyers_get_age ): ?>

							<span class="directorist-listing-details-age">

								<?php printf( '%s %s', esc_html__( 'Age', 'dlawyers' ), esc_html( $dlawyers_get_age ) ); ?>
							
							</span>

						<?php endif;?>

					</div>

					<?php if ( $dlawyers_display_reviews && $dlawyers_rating_count ): ?>

						<div class="rating-wrappers">

							<div class="directorist-rated-stars">
								
								<strong class="avg-rating"><?php echo esc_html( $listing->get_rating_count() ); ?></strong>
								
								<?php Directorist_Support::get_rating_stars_html( $listing->get_rating_count() ); ?>
							
							</div>

							<div class="number-of-review">

								<a href="#directorist-review-block"><?php echo wp_kses_post( $dlawyers_review_count_html ); ?></a>

							</div>

						</div>

					<?php endif;?>

					<?php if ( $dlawyers_display_consultation && $dlawyers_get_consultation ): ?>

						<?php printf( '<div class="directorist-listing-card-checkbox"><i class="directorist-icon %s"></i> Free Consultation</div>', esc_attr( $dlawyers_display_consultation_icon ), $dlawyers_display_consultation_text );?>
					
					<?php endif;?>

					<?php if ( $dlawyers_display_content && $listing->get_contents() ): ?>

						<div class="directorist-listing-details__text">

							<p><?php echo wp_kses_post( $listing->get_contents() ); ?></p>

						</div>

					<?php endif;?>

					<?php $listing->quick_info_template(); ?>

				</div>

			</div>

		</div>

		<div class="theme-listing-details-card__right">

			<?php $listing->quick_actions_template(); ?>

			<div class="directorist-modal directorist-modal-js directorist-fade directorist-report-abuse-modal">

				<div class="directorist-modal__dialog">

					<div class="directorist-modal__content">

						<form id="directorist-report-abuse-form">

							<div class="directorist-modal__header">

								<h3 class="directorist-modal-title" id="directorist-report-abuse-modal__label"><?php esc_html_e('Report Abuse', 'dlawyers'); ?></h3>

								<a href="#" class="directorist-modal-close directorist-modal-close-js"><span aria-hidden="true">&times;</span></a>

							</div>

							<div class="directorist-modal__body">

								<div class="directorist-form-group">

									<label for="directorist-report-message"><?php esc_html_e( 'Your Complain', 'dlawyers' ); ?><span class="directorist-report-star">*</span></label>

									<textarea class="directorist-form-element" id="directorist-report-message" rows="3" placeholder="<?php esc_attr_e( 'Message...', 'dlawyers' ); ?>" required></textarea>

								</div>

								<div id="directorist-report-abuse-g-recaptcha"></div>

								<div id="directorist-report-abuse-message-display"></div>

							</div>

							<div class="directorist-modal__footer">

								<button type="submit" class=" directorist-btn directorist-btn-primary directorist-btn-sm"><?php esc_html_e( 'Submit', 'dlawyers' ); ?></button>

							</div>

						</form>

					</div>

				</div>

			</div>

		</div>
		
	</div>

</div>