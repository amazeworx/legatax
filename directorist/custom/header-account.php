<?php

/**
 * @author  wpWax
 * @since   1.0
 * @version 1.0
 */

use wpWax\dLawyers\Helper;

if (atbdp_is_page('login') || atbdp_is_page('registration')) {
  return;
}

?>

<li class="theme-header-action__authentication">

  <?php if (!is_user_logged_in()) : ?>

    <div class="theme-header-action__authentication--login">

      <div class="login_modal" data-toggle="modal" data-target="#theme-login-modal"><button type="button" class="bg-indigo-700 hover:bg-violet-700 text-white px-6 py-2 rounded flex items-center flex-nowrap"><i class="themeicon themeicon-user"></i> <span class="whitespace-nowrap inline-block ml-2">Masuk</span></button></div>

    </div>

  <?php
  else :

    Helper::get_template_part('directorist/custom/content-header-avatar');

  endif;
  ?>

</li>